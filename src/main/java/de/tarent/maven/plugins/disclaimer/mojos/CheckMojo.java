/*
 * maven-disclaimer-plugin,
 * Maven Plugin to check or write Disclaimer in Java-Sources
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'maven-disclaimer-plugin'
 * Signature of Elmar Geese, 20 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.maven.plugins.disclaimer.mojos;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;

import de.tarent.maven.plugins.disclaimer.utils.Disclaimer;

/**
 * @author David Goemans, tarent GmbH
 * @goal check
 */
public class CheckMojo extends AbstractMojo{
	/**
	 * The Project
	 * 
	 * @parameter expression="${project}"
	 */
	MavenProject pom;
	
	public void execute() throws MojoExecutionException, MojoFailureException {
		// TODO Auto-generated method stub
		Log log = getLog();
		Disclaimer disclaimer = new Disclaimer(pom, log);
		ArrayList<String> errors = disclaimer.checkIfAllValuesSet();
		String encodingError = disclaimer.setEncoding(pom);
		if (errors.size() < 1) {
			if (disclaimer.checkAll()) {
				log.info("------------------------------------------------------------------------");
				log.info("Finished Disclaimer-Check: All Java-Files contain a Disclaimer!");
				log.info("------------------------------------------------------------------------");
			} else {
				log.info("------------------------------------------------------------------------");
				log.info("Finished Disclaimer-Check: There are Java-Files without a Disclaimer!");
				log.info("------------------------------------------------------------------------");
			}
		}else if(encodingError != null){
			log.error(encodingError);
		}else{
			Iterator<String> iter = errors.iterator();
			while(iter.hasNext()){
				log.error(iter.next());
			}
			log.error("------------------------------------------------------------------------");
			log.error("Checking sources for disclaimer failed!");
			log.error("------------------------------------------------------------------------");
		}		
	}

}
