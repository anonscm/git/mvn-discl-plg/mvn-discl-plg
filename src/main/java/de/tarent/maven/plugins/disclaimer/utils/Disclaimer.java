/*
 * maven-disclaimer-plugin,
 * Maven Plugin to check or write Disclaimer in Java-Sources
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'maven-disclaimer-plugin'
 * Signature of Elmar Geese, 20 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.maven.plugins.disclaimer.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.maven.model.Plugin;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.Xpp3Dom;

import java.util.regex.Matcher;

import de.tarent.maven.plugins.disclaimer.utils.FileHandler;

/**
 * 
 * @author David Goemans, tarent GmbH
 *
 */
public class Disclaimer {
	/**
	 * Regex is used to Replace a not valid GPL-Header
	 */
	private static String regexReplace = "\\/\\*(.|\\\n)+?Copyright(.|\\\n)+?\\*\\/(.|\\\n)+?";
	
	/**
	 * Regex is used to check if a unvalid GPL-Header exists in File
	 */
	private static String regexMatches = "\\/\\*(.|\\\n)+?Copyright(.|\\\n)+?\\*\\/(.|\\\n)+";
	
	/**
	 * String with disclaimer, which should be set
	 */
	private StringBuffer disclaimer;
	/**
	 * Source-Folder of Java-Files
	 */
	private File sourceFolder;
	/**
	 * Loger
	 */
	private Log log;
	/**
	 * Pom
	 */
	private MavenProject pom;
	
	/**
	 * Properties
	 */
	private Properties properties;
	
	/**
	 * The Encoding of the ProjectFiles
	 */
	public String encoding;
	
	private Disclaimer(){
	}
	
	/**
	 * Constructor which uses pom and log for instantiation
	 * 
	 * @param pom	Pom of the Maven-Projects
	 * @param log	Logger des Maven-Projects
	 */
	public Disclaimer(MavenProject pom, Log log){
//		Locale.setDefault(Locale.UK);
		this.sourceFolder = new File("src");
		this.log = log;
		this.pom = pom;
		
		SimpleDateFormat format = new SimpleDateFormat("d MMMM yyyy", Locale.US);
		this.disclaimer = new StringBuffer("/*\n" +
				" * " + pom.getName() + ",\n" +
				" * " + pom.getDescription() + "\n" +
				getCopyrightString(pom) +
				" *\n" +
				" * This program is free software; you can redistribute it and/or\n" +
				" * modify it under the terms of the GNU General Public License,version 2\n" +
				" * as published by the Free Software Foundation.\n" +
				" *\n" +
				" * This program is distributed in the hope that it will be useful,\n" +
				" * but WITHOUT ANY WARRANTY; without even the implied warranty of\n" +
				" * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n" +
				" * GNU General Public License for more details.\n" +
				" *\n" +
				" * You should have received a copy of the GNU General Public License\n" +
				" * along with this program; if not, write to the Free Software\n" +
				" * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA\n" +
				" * 02110-1301, USA.\n" +
				" *\n" +
				" * tarent GmbH., hereby disclaims all copyright\n" +
				" * interest in the program '" + pom.getName() + "'\n" +
				" * Signature of Elmar Geese, " + format.format(new Date()) + "\n" +
				" * Elmar Geese, CEO tarent GmbH.\n" +
				" */\n");
		this.properties = pom.getProperties();
		if(properties.containsKey("disclaimer")){
			String tempDisclaimer = properties.get("disclaimer").toString();
			tempDisclaimer.replaceAll("\\$name", pom.getName());
			this.disclaimer = new StringBuffer(tempDisclaimer + "\n");
		}
	}
	
	/**
	 * Method to get the Root-File (Sourcefolder)
	 * 
	 * @return	The Sourcefolder of the MavenProject
	 */
	public File getRootFile(){
		return this.sourceFolder;
	}
	
	/**
	 * Method to Check all Java-Sources for disclaimer.
	 * 
	 * @return	True if all sources have a disclaimer, otherwise false
	 */
	public boolean checkAll(){
		File f = getRootFile();
		boolean state = true;
		if(f.isDirectory()){
			if(!(this.checkFolder(f))){
				state = false;
			}
		}else if(f.getAbsolutePath().toLowerCase().endsWith("java")){
			if(!(this.check(f))){
				state = false;	
			}
		}
		return state;
	}
	
	/**
	 * Method to set a disclaimer for all Java-Files
	 */
	public void disclaimAll(){
		// TODO Write
		File f = getRootFile();
		if(f.isDirectory()){
			this.disclaimFolder(f);
		}else if(f.getAbsolutePath().toLowerCase().endsWith("java")){
			this.disclaim(f);
		}
	}
	
	/**
	 * Method to check a Folder for disclaimer.
	 * 
	 * @param folder	Folder which should be checked
	 * @return	True if all files of the foder have a disclaimer, otherwise false
	 */
	private boolean checkFolder(File folder){
		boolean state = true;
		if(folder.isDirectory()){
			File[] files = folder.listFiles();
			for(int i = 0; i < files.length; i++){
				File tmp = files[i];
				if(tmp.isDirectory()){
					if(!(checkFolder(tmp))){
						state = false;
					}
				}else if(tmp.getAbsolutePath().toLowerCase().endsWith("java")){
					if(!(check(tmp))){
						state = false;
					}
				}
			}
		}
		return state;
	}
	
	/**
	 * Disclaims all Java-Files in a folder
	 * @param folder	Folder which should be disclaimed
	 */
	private void disclaimFolder(File folder){
		if(folder.isDirectory()){
			File[] files = folder.listFiles();
			for(int i = 0; i < files.length; i++){
				File tmp = files[i];
				if(tmp.isDirectory()){
					disclaimFolder(tmp);
				}else if(tmp.getAbsolutePath().toLowerCase().endsWith("java")){
					disclaim(tmp);
				}
			}
		}
	}
	
	/**
	 * Disclaims a JavaFile
	 * @param javaFile Java-File which should be disclaimed
	 */
	private void disclaim(File javaFile){
		if(!this.check(javaFile)){
			StringBuffer content = FileHandler.readContent(javaFile, encoding);
			StringBuffer newContent = new StringBuffer();
			String temp;
			
			// Cut the content, if a it's longer than 1500 characters
			if(content.length() >= 2000){
				temp = content.substring(0, 2000);
			}else{
				temp = content.toString();
			}
			
			// Check if file already contains a header
			if(temp.toString().matches(regexMatches)){
				log.info("Regex Matched");
				Pattern p = Pattern.compile(regexReplace);
				Matcher m = p.matcher(content.toString());
				newContent = new StringBuffer(m.replaceFirst(Matcher.quoteReplacement(disclaimer.toString())));
			}else{
				log.info("Regex didn't match");
				newContent.append(disclaimer + "\n");
				newContent.append(content);
			}
			
			FileHandler.writeContent(javaFile, newContent, this.encoding);
			this.log.info("Added disclaimer to file: " + javaFile.getAbsolutePath());
		}
	}
	
	/**
	 * Checks a Java File for disclaimer
	 * @param javaFile Java-File which should be disclaimed
	 * @return True if it contains disclaimer.
	 */
	private boolean check(File javaFile){
		String content = FileHandler.readContent(javaFile, this.encoding).toString();
		if(content.contains(this.disclaimer)){
			this.log.info("Checked file: " + javaFile.getAbsolutePath() + ": true");
			return true;	
		}else{
			this.log.info("Checked file: " + javaFile.getAbsolutePath() + ": false");
			return false;			
		}
	}
	
	/**
	 * This Method gets the Copyright-String by Pom (JAR- or WAR-Plugin) if it isn't set it uses
	 * a default String
	 * @param pom	Pom of Project
	 * @return	Copyright-String
	 */
	private static String getCopyrightString(MavenProject pom){
		String copyRight = null;
		try{
			List<Plugin> list = pom.getBuildPlugins();
	    	Iterator<Plugin> itPlugin = list.iterator();
	    	while(itPlugin.hasNext()){
	    		Plugin plugin = itPlugin.next();
	    		if(plugin.getArtifactId().equalsIgnoreCase("maven-jar-plugin")
	    				|| plugin.getArtifactId().equalsIgnoreCase("maven-war-plugin")){
	    			Xpp3Dom config = (Xpp3Dom)(plugin.getConfiguration());
	    			copyRight = " * " + config.getChild("archive").getChild("manifestEntries").getChild("version_copyright").getValue().toString() + "\n";
	    		}
	    	}
		}catch(Exception e){
			;
		}finally{
			if(copyRight == null){
				copyRight = " * Copyright (C) 2000-" + (new Date().getYear()+1900) + " tarent GmbH\n";
			}
			return copyRight;
		}
	}
	
	/**
	 * This method sets the Encoding, which is used by the FileHandler, to the value which is definde in maven-compiler-plugin-configuration.
	 * This value is needed by the FileHandler to secure, that all character are read and written right (special-characters, too). It returns
	 * an error-message if the maven-compiler plugin isn't configured right.
	 * @param pom MavenProject of the Project (pom.xml) in which the encoding is defined
	 * @return error-message if maven-compiler-plugin isn't configured
	 */
	public String setEncoding(MavenProject pom){
		String result = null;
		try{
			List<Plugin> list = pom.getBuildPlugins();
	    	Iterator<Plugin> itPlugin = list.iterator();
	    	while(itPlugin.hasNext()){
	    		Plugin plugin = itPlugin.next();
	    		if(plugin.getArtifactId().equalsIgnoreCase("maven-compiler-plugin")){
	    			Xpp3Dom config = (Xpp3Dom)(plugin.getConfiguration());
	    			this.encoding = config.getChild("encoding").getValue().toString();
	    		}
	    	}
		}catch(Exception e){
			result = "maven-compiler-plugin not defined or no encoding defined!";
		}finally{
			return result;
		}
	}
	
	/**
	 * This method returns an ArrayList with error-Strings if one or more nessecery values aren't set
	 * @param pom Pom of the Project
	 * @return ArrayList with error-Strings
	 */
	public ArrayList<String> checkIfAllValuesSet(){
		ArrayList<String> errors = new ArrayList<String>();
		if(pom.getArtifact() == null){
			errors.add("artifactId isn't set for Project");
		}
		if(pom.getDescription() == null){
			errors.add("description isn't set for Project");
		}
		if(pom.getName() == null || pom.getName().contains("Unnamed")){
			errors.add("name isn't set for Project");
		}
		return errors;
	}
}
