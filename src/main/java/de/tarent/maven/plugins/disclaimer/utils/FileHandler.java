/*
 * maven-disclaimer-plugin,
 * Maven Plugin to check or write Disclaimer in Java-Sources
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'maven-disclaimer-plugin'
 * Signature of Elmar Geese, 20 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.maven.plugins.disclaimer.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * @author David Goemans, tarent GmbH
 */
public class FileHandler {
	
	public static StringBuffer readContent(File f, String encoding){
		if(encoding == null)
			throw new RuntimeException("The parameter 'encoding' was not defined! Please check the configuration.");
		
		StringBuffer content = new StringBuffer();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(f), encoding));
			int tmp = -1;
			while((tmp = reader.read()) != -1){
				content.append((char)tmp);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return content;
	}
	
	public static void writeContent(File f, StringBuffer content, String encoding){
		if(encoding == null)
			throw new RuntimeException("The parameter 'encoding' was not defined! Please check the configuration.");
		
		try {
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), encoding));
			out.write(content.toString().toCharArray());
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void copyContent(File f1, File f2, String encoding){
		if(encoding == null)
			throw new RuntimeException("The parameter 'encoding' was not defined! Please check the configuration.");
		
		writeContent(f2, readContent(f1, encoding), encoding);
	}
	

}
